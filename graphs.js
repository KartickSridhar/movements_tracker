// var config = {
//   apiKey: "AIzaSyCqmZjnLy1Pm0_m470_ryGpR_NUuruuXjA",
//   authDomain: "funfacts-ca046.firebaseapp.com",
//   databaseURL: "https://funfacts-ca046.firebaseio.com",
//   projectId: "funfacts-ca046",
//   storageBucket: "funfacts-ca046.appspot.com",
//   messagingSenderId: "817822935753"
// };
// firebase.initializeApp(config);

var xhr = new XMLHttpRequest();
// var ref = firebase.database().ref("/params");
xhr.onreadystatechange = function(){
//ref.once("value").then(function(data){
if(xhr.readyState === 4 && xhr.status === 200){
var params = JSON.parse(xhr.responseText);
//var params = data.toJSON();
//var params = JSON.parse(paramsjson);
var rollValues = [];
var pitchValues = [];
var yawValues  = [];
var XaxisValues = [];
var YaxisValues = [];
var ZaxisValues = [];
var timeIntervals = [];
var count = 0;
var probcalm;
var degree_restlessness;
var num_intervals;

for(var i = 0; i < params.length; i++){

timeIntervals.push(params[i].Time);


rollValues.push(params[i].Roll);
var graphData = {
           labels: timeIntervals,
           datasets: [{
                   label: "Roll",
                   fill: false,
                   lineTension: 0.1,
                   backgroundColor: "rgba(60,90,153,0.75)",
                   borderColor: "rgba(59,89,152,1)",
                   pointHoverBackgroundColor: "rgba(60,90,153,1)",
                   pointHoverBorderColor: "rgba(60,90,152,1)",
                   data: rollValues
}]
}


var canvas_element = document.getElementById("roll");
var canvas  = new Chart(canvas_element,{
                 type: 'line',
                 data:  graphData
});
canvas_element.innerHTML = canvas;


pitchValues.push(params[i].Pitch);
var pitchData = {
           labels: timeIntervals,
           datasets: [{
                   label: "Pitch",
                   fill: false,
                   lineTension: 0.1,
                   backgroundColor: "rgba(60,153,90,0.75)",
                   borderColor: "rgba(59,152,89,1)",
                   pointHoverBackgroundColor: "rgba(60,153,90,1)",
                   pointHoverBorderColor: "rgba(60,152,90,1)",
                   data: pitchValues
}]
}


var pitch_element = document.getElementById("pitch");
var pitch  = new Chart(pitch_element,{
                 type: 'line',
                 data:  pitchData
});
pitch_element.innerHTML = pitch;

yawValues.push(params[i].Yaw);
var yawData = {
        labels: timeIntervals,
        datasets: [{
                label: "Yaw",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(153,60,90,0.75)",
                borderColor: "rgba(152,59,89,1)",
                pointHoverBackgroundColor: "rgba(153,60,90,1)",
                pointHoverBorderColor: "rgba(152,60,90,1)",
                data: yawValues
}]
}
var yaw_element = document.getElementById("yaw");
var yaw = new Chart(yaw_element,{
                type: 'line',
                data: yawData

});
yaw_element.innerHTML = yaw;

XaxisValues.push(params[i].X);
var XaxisData = {
           labels: timeIntervals,
           datasets: [{
                   label: "X",
                   fill: false,
                   lineTension: 0.1,
                   backgroundColor: "rgba(60,90,153,0.75)",
                   borderColor: "rgba(59,89,152,1)",
                   pointHoverBackgroundColor: "rgba(60,90,153,1)",
                   pointHoverBorderColor: "rgba(60,90,152,1)",
                   data: XaxisValues
}]
}


var x = document.getElementById("Xaxis");
var xaxis  = new Chart(x,{
                 type: 'line',
                 data:  XaxisData
});
x.innerHTML = xaxis;

YaxisValues.push(params[i].Y);
var YaxisData = {
           labels: timeIntervals,
           datasets: [{
                   label: "Y",
                   fill: false,
                   lineTension: 0.1,
                   backgroundColor: "rgba(60,153,90,0.75)",
                   borderColor: "rgba(59,152,89,1)",
                   pointHoverBackgroundColor: "rgba(60,153,90,1)",
                   pointHoverBorderColor: "rgba(60,152,90,1)",
                   data: YaxisValues
}]
}


var y = document.getElementById("Yaxis");
var yaxis  = new Chart(y,{
                 type: 'line',
                 data:  YaxisData
});
y.innerHTML = yaxis;

ZaxisValues.push(params[i].Z);
var ZaxisData = {
        labels: timeIntervals,
        datasets: [{
                label: "Z",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(153,60,90,0.75)",
                borderColor: "rgba(152,59,89,1)",
                pointHoverBackgroundColor: "rgba(153,60,90,1)",
                pointHoverBorderColor: "rgba(152,60,90,1)",
                data: ZaxisValues
}]
}

var z = document.getElementById("Zaxis");
var zaxis = new Chart(z,{
                type: 'line',
                data:  ZaxisData

});
z.innerHTML = zaxis;
}

for(var j = 0; j < pitchValues.length - 1; j = j + 1){
if((Math.abs(pitchValues[j+1] - pitchValues[j]) == 1) || (Math.abs(pitchValues[j+1] - pitchValues[j]) == 0)){
count = count + 1;
console.log(count);
}
}
num_intervals = pitchValues.length - 1;
probcalm = count/num_intervals;
degree_restlessness = 1 - probcalm;
console.log(num_intervals);
console.log(count);
console.log(probcalm);
console.log(degree_restlessness);
var result = document.getElementById("result");
result.innerHTML = "The person's degree of restlessness is " + degree_restlessness
}
}
xhr.open('GET',"userorientation.txt");
xhr.send();
