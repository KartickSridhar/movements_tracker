from __future__ import division
import pymysql
import cgi
import cgitb
import os
import time
import json
from pyfcm import FCMNotification
from firebase import firebase
from sense_hat import SenseHat
cgitb.enable()
sense = SenseHat()
fcm_push = FCMNotification("AAAAvmoLqsk:APA91bFJbgvbu6A7GZuryrUfKfH8eZXfwsQhmjT73RfL7pEZNC0y5rdOZ-p-wK_BxCgAeXpvxO75T09z1n-ZYTMZMyrTe2uhwLOC-nZM_t6RcDSqhCd5P-11tYoHKLDEPZldwaqN4lVh");
firebase = firebase.FirebaseApplication("https://funfacts-ca046.firebaseio.com",None);

Time = 0
device_token = "eLVEofkmEWc:APA91bF46Kjka_tTJozJ4MgsHD4sDz05Xxa6udtWuU3W9C3hGB7rLnGSKyVodYxn7n1q5Cbe4u_RMVG8z21hbl4mD7vaCmXcq0tJJa6VG8-bq582neqezRR6PKbdEP_hQZoBnSG5dJuk" 
message_title = "Pi update"
message_body = "Please get up, it's time to take a break"
new_user = 'Kartick'
params = []
valroll = []
valpitch = []
valyaw = []
count = 0
while Time < 20:
    ac = sense.get_accelerometer_raw()
    orn = sense.get_orientation()

    x = ac['x']
    y = ac['y']
    z = ac['z']

    x = round(x, 0)
    y = round(y, 0)
    z = round(z, 0)

    yaw = orn['yaw']
    pitch = orn['pitch']
    roll = orn['roll']

    yaw = int(round(yaw, 0))
    pitch = int(round(pitch, 0))
    roll = int(round(roll, 0))

    valyaw.append(yaw)
    valpitch.append(pitch)
    valroll.append(roll)

    #params = {}
    #params['Time'] = Time
    #params['Roll'] = roll
    #params['Pitch'] = pitch
    #params['Yaw'] = yaw
    #params['X'] = x
    #params['Y'] = y
    #params['Z'] = z

    params.append({"Time": Time,"Roll": roll,"Pitch": pitch,"Yaw": yaw,"X": x,"Y": y,"Z": z})
    #postparams = json.dumps(params,default=str)


    db = pymysql.connect("localhost","rob","robby","positionDetector")
    cursor = db.cursor()

    try:
        cursor.execute("""INSERT INTO orientation  VALUES(%s, %s, %s, %s, %s, %s, %s)""",(Time, roll, pitch, yaw, x, y, z))

        print("Data inserted succesfully!!")
        db.commit()
 
     	if Time == 10:
                 breakalert = fcm_push.notify_single_device(device_token,message_title,message_body)
                 print (breakalert)
        if Time == 14 and (abs(valroll[7] - valroll[6]) <= 1) and (abs(valpitch[7] -  valpitch[6]) <= 1) and (abs(valyaw[7] - valyaw[6]) <= 1):
	         timeoutalert = fcm_push.notify_single_device(device_token,"Pi timeout update","Your break time is over, please get back to work!!!")
		 print (timeoutalert)
	if Time == 18:
		 postdata = firebase.post('/params',params)
		 print (postdata)

    except:
        db.rollback()
        db.close()

    messg = "Accelerometer values are {0}, {1}, {2}".format(x,y,z)

    msg   = "Roll, pitch and yaw are  {0}, {1}, {2}".format(roll,pitch,yaw)

    print(messg)

    print(msg)
    time.sleep(2)
    Time = Time + 2
#for data in range(len(valroll)-1):
#	if abs(valroll[data + 1] - valroll[data]) <= 1:
#        	count = count + 1
#probcalm = count/(len(valroll) - 1)
#postmessage = firebase.post('/params',probcalm)
#print (postmessage)








